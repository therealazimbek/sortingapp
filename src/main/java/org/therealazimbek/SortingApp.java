package org.therealazimbek;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Hello world!
 *
 */
public class SortingApp
{
    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    public static void main(String[] args)
    {
        logger.trace("Welcome to Simple Sorting App!");
        List<Integer> numbers = new ArrayList<Integer>();

        if (args == null || args.length == 0) {
            logger.error("Empty stack is not allowed!");
            throw new IllegalArgumentException("Empty stack is not allowed!");
        } else if (args.length > 10) {
            logger.error("No more than 10 numbers are allowed!");
            throw new IllegalArgumentException("No more than 10 numbers are allowed!");
        }

        for (String s: args) {
            int n;
            try {
                n = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                logger.error("Invalid input!");
                throw new IllegalArgumentException(e);
            }
            numbers.add(n);
        }

        Collections.sort(numbers);

        for (int n: numbers) {
            System.out.print(n + " ");
        }
        System.out.println();
        logger.info("Goodbye!");
    }
}
