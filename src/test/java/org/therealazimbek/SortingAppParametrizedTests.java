package org.therealazimbek;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppParametrizedTests {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new String[] {"10", "1", "9", "8", "7", "6", "5", "4", "2", "0"},
                        null },
                { new String[] {"13", "21", "89", "68", "87", "56", "45", "34", "288", "120"},
                        new String[] {} },
                { new String[] {"123", "251", "894", "235", "4566", "456", "0", "34", "636", "3243"},
                        new String[] {"ii"} },
                { new String[] {"133", "2134", "869", "786", "46", "58566", "445", "456", "3633", "3443"},
                        new String[] {"133", "2134", "869", "786", "46", "58566", "445", "456", "3633", "3443", "1"} },
                { new String[] {"153", "23451", "989", "6357", "68", "345", "123", "3457434", "36", "16720"},
                        new String[] {"u"} }
        });
    }

    private final String[] input;
    private final String[] invalidInput;


    public SortingAppParametrizedTests(String[] input, String[] invalidInput) {
        this.input = input;
        this.invalidInput = invalidInput;
    }

    @Test
    public void testWithDifferentCases() {
        SortingApp.main(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithErrorCases() {
        SortingApp.main(invalidInput);
    }
}
