package org.therealazimbek;

import org.junit.Test;

/**
 * Unit test for Sorting App.
 */
public class SortingAppTest {

    @Test(expected = IllegalArgumentException.class)
    public void testWithNullCase()
    {
        SortingApp.main(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithZeroNumbersCase()
    {
        SortingApp.main(new String[]{});
    }

    @Test()
    public void testWithOneNumbersCase()
    {
        SortingApp.main(new String[]{"1000"});
    }

    @Test
    public void testWithMoreTenNumbersCase()
    {
        SortingApp.main(new String[] {"1", "9", "8", "7", "6", "5", "4", "2", "0", "100"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithMoreThanTenNumbersCase()
    {
        SortingApp.main(new String[] {"10", "1", "9", "8", "7", "6", "5", "4", "2", "0", "100"});
    }
}
